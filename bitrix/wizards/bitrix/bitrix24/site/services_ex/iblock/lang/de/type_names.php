<?
$MESS ['PHOTOS_SECTION_NAME'] = "Alben";
$MESS ['EVENTS_TYPE_NAME'] = "Kalender";
$MESS ['EVENTS_SECTION_NAME'] = "Kalender";
$MESS ['STRUCTURE_TYPE_NAME'] = "Firmenstruktur";
$MESS ['LIBRARY_TYPE_NAME'] = "Dokumente";
$MESS ['STRUCTURE_ELEMENT_NAME'] = "Elemente";
$MESS ['SERVICES_ELEMENT_NAME'] = "Elemente";
$MESS ['EVENTS_ELEMENT_NAME'] = "Ereignisse";
$MESS ['LIBRARY_ELEMENT_NAME'] = "Dateien";
$MESS ['LIBRARY_SECTION_NAME'] = "Ordner";
$MESS ['NEWS_TYPE_NAME'] = "News";
$MESS ['NEWS_ELEMENT_NAME'] = "News";
$MESS ['PHOTOS_TYPE_NAME'] = "Fotogalerie";
$MESS ['PHOTOS_ELEMENT_NAME'] = "Fotos";
$MESS ['STRUCTURE_SECTION_NAME'] = "Bereiche";
$MESS ['SERVICES_SECTION_NAME'] = "Bereiche";
$MESS ['SERVICES_TYPE_NAME'] = "Dienste";
?>