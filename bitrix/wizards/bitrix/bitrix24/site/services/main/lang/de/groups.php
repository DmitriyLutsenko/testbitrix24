<?
$MESS["EMPLOYEES_GROUP_NAME"] = "Mitarbeiter";
$MESS["EMPLOYEES_GROUP_DESC"] = "Alle Mitarbeiter, die auf dem Portal registriert sind.";

$MESS["PERSONNEL_DEPARTMENT_GROUP_NAME"] = "Personalabteilung";
$MESS["PERSONNEL_DEPARTMENT_GROUP_DESC"] = "Mitarbeiter der Personalabteilung";

$MESS["DIRECTION_GROUP_NAME"] = "Geschдftsfьhrung";
$MESS["DIRECTION_GROUP_DESC"] = "Geschдftsfьhrung";

$MESS["PORTAL_ADMINISTRATION_GROUP_NAME"] = "Portal-Administratoren";
$MESS["PORTAL_ADMINISTRATION_GROUP_DESC"] = "Administratoren des Portals mit Zugriff auf alle Portal-Services";

$MESS["ADMIN_SECTION_GROUP_NAME"] = "User des administrativen Bereichs";
$MESS["ADMIN_SECTION_GROUP_DESC"] = "User dieser Gruppe dьrfen mit dem administrativen Panel arbeiten.";

$MESS["CREATE_GROUPS_GROUP_NAME"] = "Projektgruppen-Administratoren";
$MESS["CREATE_GROUPS_GROUP_DESC"] = "User dieser Gruppe dьrfen neue Projektgruppen anlegen.";

$MESS["MARKETING_AND_SALES_GROUP_NAME"] = "Marketing und Vertrieb";
$MESS["MARKETING_AND_SALES_GROUP_DESC"] = "Mitarbeiter der Vertriebsabteilungen";

$MESS["SUPPORT_GROUP_NAME"] = "Support";
$MESS["SUPPORT_GROUP_DESC"] = "Mitarbeiter des Technischen Supports";
$MESS["INTEGRATOR_GROUP_NAME"] = "Bitrix24 Partner";

$MESS["SALE_USER_GROUP_SHOP_ADMIN_NAME"] = "Onlineshop-Administratoren";
$MESS["SALE_USER_GROUP_SHOP_MANAGER_NAME"] = "Onlineshop-Mitarbeiter";
$MESS["SALE_USER_GROUP_SHOP_ADMIN_DESC"] = "Nutzergruppe, die Onlineshop-Einstellungen bearbeiten kann";
$MESS["SALE_USER_GROUP_SHOP_MANAGER_DESC"] = "Nutzergruppe, die Onlineshop-Funktionen nutzen kann";
?>