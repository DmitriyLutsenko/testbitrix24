<?
$MESS["COMMENTS_GROUP_NAME"] = "Foren fьr Kommentare";
$MESS["GENERAL_GROUP_NAME"] = "Allgemeine Foren";
$MESS["HIDDEN_GROUP_NAME"] = "Ausgeblendete Foren";
$MESS["SONET_GROUP_DESCRIPTION_NEW_1"] = "Nutzen Sie diese Gruppe, um an aktuell laufenden Projekten zu arbeiten. Neue Teilnehmer kцnnen darauf zugreifen, nachdem dies vom Gruppenbesitzer oder von Gruppenmoderatoren genehmigt wird. Diese Gruppe ist fьr alle sichtbar.";
$MESS["SONET_GROUP_DESCRIPTION_NEW_2"] = "Nutzen Sie diese Gruppe, um mit geschlossenen bzw. vertraulichen Informationen zu arbeiten (bspw. im Finanzbereich). Nur die eingeladenen Personen haben den Zugriff. Diese Gruppe ist nur fьr die Gruppenteilnehmer sichtbar.";
$MESS["SONET_GROUP_DESCRIPTION_NEW_3"] = "Diese Gruppe steht jedem zur Verfьgung. Hier kцnnen beliebige Themen besprochen werden: Sport, Musik, Reisen, Urlaub. Diese Gruppe ist fьr alle sichtbar, jeder kann der Gruppe beitreten, eine Einladung ist nicht erforderlich.";
$MESS["SONET_GROUP_DESCRIPTION_NEW_41"] = "Diese geschlossene sichtbare Gruppe ist fьr externe Nutzer, zu denen Geschдftspartner, Kunden sowie andere Nutzer gehцren kцnnen, die keine Mitarbeiter Ihres Unternehmens sind. Eine Einladung ist erforderlich, um dieser Gruppe beizutreten. Die externen Nutzer kцnnen lediglich diese eine Gruppe sehen, sie haben keinen Zugriff auf andere Informationen in Ihrem Bitrix24 und sehen diese auch nicht.";
$MESS["SONET_GROUP_KEYWORDS_NEW_1"] = "Projekte, Routineprozesse, Recherchen";
$MESS["SONET_GROUP_KEYWORDS_NEW_2"] = "Markt, Merchandising, Geld, Finanzen, Ausschreibung";
$MESS["SONET_GROUP_KEYWORDS_NEW_3"] = "Sport, Reisen, SpaЯ, FuЯball, Musik";
$MESS["SONET_GROUP_KEYWORDS_NEW_4"] = "Projekte, Freelancer, Geschдftspartner";
$MESS["SONET_GROUP_NAME_NEW_1"] = "Geschlossene sichtbare Gruppe";
$MESS["SONET_GROUP_NAME_NEW_2"] = "Geschlossene, nicht sichtbare Gruppe ";
$MESS["SONET_GROUP_NAME_NEW_3"] = "Offene sichtbare Gruppe";
$MESS["SONET_GROUP_NAME_NEW_4"] = "Extranet: Gruppe fьr externe Nutzer";
$MESS["SONET_GROUP_SUBJECT_0"] = "Marketing und Vertrieb ";
$MESS["SONET_TASK_DESCRIPTION_1"] = "Fьllen Sie die Profildaten aus und laden Sie Ihr Foto hoch";
$MESS["SONET_TASK_DESCRIPTION_2"] = "Neue Mitarbeiter auf das Intranet Portal einladen";
$MESS["SONET_TASK_TITLE_1"] = "Profildaten ausfьllen";
$MESS["SONET_TASK_TITLE_2"] = "Neue Mitarbeiter einladen";
?>