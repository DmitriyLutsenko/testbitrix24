<?php
/**
 * Bitrix Framework
 * @package bitrix
 * @subpackage main
 * @copyright 2001-2015 Bitrix
 */
namespace Bitrix\Main\Config;

use Bitrix\Main;

class Option
{
	const CACHE_DIR = "b_option";

	protected static $options = array();

	/**
	 * Returns a value of an option.
	 *
	 * @param string $moduleId The module ID.
	 * @param string $name The option name.
	 * @param string $default The default value to return, if a value doesn't exist.
	 * @param bool|string $siteId The site ID, if the option differs for sites.
	 * @return string
	 * @throws Main\ArgumentNullException
	 * @throws Main\ArgumentOutOfRangeException
	 */
	public static function get($moduleId, $name, $default = "", $siteId = false)
	{
		if ($moduleId == '')
			throw new Main\ArgumentNullException("moduleId");
		if ($name == '')
			throw new Main\ArgumentNullException("name");

		if (!isset(self::$options[$moduleId]))
		{
			static::load($moduleId);
		}

		if ($siteId === false)
		{
			$siteId = static::getDefaultSite();
		}

		$siteKey = ($siteId == ""? "-" : $siteId);

		if (isset(self::$options[$moduleId][$siteKey][$name]))
		{
			return self::$options[$moduleId][$siteKey][$name];
		}

		if (isset(self::$options[$moduleId]["-"][$name]))
		{
			return self::$options[$moduleId]["-"][$name];
		}

		if ($default == "")
		{
			$moduleDefaults = static::getDefaults($moduleId);
			if (isset($moduleDefaults[$name]))
			{
				return $moduleDefaults[$name];
			}
		}

		return $default;
	}

	/**
	 * Returns the real value of an option as it's written in a DB.
	 *
	 * @param string $moduleId The module ID.
	 * @param string $name The option name.
	 * @param bool|string $siteId The site ID.
	 * @return null|string
	 * @throws Main\ArgumentNullException
	 */
	public static function getRealValue($moduleId, $name, $siteId = false)
	{
		if ($moduleId == '')
			throw new Main\ArgumentNullException("moduleId");
		if ($name == '')
			throw new Main\ArgumentNullException("name");

		if (!isset(self::$options[$moduleId]))
		{
			static::load($moduleId);
		}

		if ($siteId === false)
		{
			$siteId = static::getDefaultSite();
		}

		$siteKey = ($siteId == ""? "-" : $siteId);

		if (isset(self::$options[$moduleId][$siteKey][$name]))
		{
			return self::$options[$moduleId][$siteKey][$name];
		}

		return null;
	}

	/**
	 * Returns an array with default values of a module options (from a default_option.php file).
	 *
	 * @param string $moduleId The module ID.
	 * @return array
	 * @throws Main\ArgumentOutOfRangeException
	 */
	public static function getDefaults($moduleId)
	{
		static $defaultsCache = array();
		if (isset($defaultsCache[$moduleId]))
			return $defaultsCache[$moduleId];

		if (preg_match("#[^a-zA-Z0-9._]#", $moduleId))
			throw new Main\ArgumentOutOfRangeException("moduleId");

		$path = Main\Loader::getLocal("modules/".$moduleId."/default_option.php");
		if ($path === false)
			return $defaultsCache[$moduleId] = array();

		include($path);

		$varName = str_replace(".", "_", $moduleId)."_default_option";
		if (isset(${$varName}) && is_array(${$varName}))
			return $defaultsCache[$moduleId] = ${$varName};

		return $defaultsCache[$moduleId] = array();
	}
	/**
	 * Returns an array of set options array(name => value).
	 *
	 * @param string $moduleId The module ID.
	 * @param bool|string $siteId The site ID, if the option differs for sites.
	 * @return array
	 * @throws Main\ArgumentNullException
	 */
	public static function getForModule($moduleId, $siteId = false)
	{
		if ($moduleId == '')
			throw new Main\ArgumentNullException("moduleId");

		if (!isset(self::$options[$moduleId]))
		{
			static::load($moduleId);
		}

		if ($siteId === false)
		{
			$siteId = static::getDefaultSite();
		}

		$result = self::$options[$moduleId]["-"];

		if($siteId <> "" && !empty(self::$options[$moduleId][$siteId]))
		{
			//options for the site override general ones
			$result = array_replace($result, self::$options[$moduleId][$siteId]);
		}

		return $result;
	}

	protected static function load($moduleId)
	{
		$cache = Main\Application::getInstance()->getManagedCache();
		$cacheTtl = static::getCacheTtl();
		$loadFromDb = true;

		if ($cacheTtl !== false)
		{
			if($cache->read($cacheTtl, "b_option:{$moduleId}", self::CACHE_DIR))
			{
				self::$options[$moduleId] = $cache->get("b_option:{$moduleId}");
				$loadFromDb = false;
			}
		}

		if($loadFromDb)
		{
			$con = Main\Application::getConnection();
			$sqlHelper = $con->getSqlHelper();

			self::$options[$moduleId] = ["-" => []];

			$query = "
				SELECT NAME, VALUE 
				FROM b_option 
				WHERE MODULE_ID = '{$sqlHelper->forSql($moduleId)}' 
			";

			$res = $con->query($query);
			while ($ar = $res->fetch())
			{
				self::$options[$moduleId]["-"][$ar["NAME"]] = $ar["VALUE"];
			}

			try
			{
				//b_option_site possibly doesn't exist

				$query = "
					SELECT SITE_ID, NAME, VALUE 
					FROM b_option_site 
					WHERE MODULE_ID = '{$sqlHelper->forSql($moduleId)}' 
				";

				$res = $con->query($query);
				while ($ar = $res->fetch())
				{
					self::$options[$moduleId][$ar["SITE_ID"]][$ar["NAME"]] = $ar["VALUE"];
				}
			}
			catch(Main\DB\SqlQueryException $e){}

			if($cacheTtl !== false)
			{
				$cache->set("b_option:{$moduleId}", self::$options[$moduleId]);
			}
		}

		/*ZDUyZmZMmY2OWNkNjZjZmI3NGY5MWEwOWZmZTQ0M2FlYWFiNzQ=*/$GLOBALS['____1080607651']= array(base64_decode('ZXhwbG9kZQ=='),base64_decode('cGF'.'jaw=='),base64_decode('bWQ1'),base64_decode(''.'Y2'.'9'.'uc3R'.'hbnQ='),base64_decode('aGFzaF9obW'.'Fj'),base64_decode('c3RyY2'.'1w'),base64_decode('aX'.'Nfb2JqZW'.'N0'),base64_decode(''.'Y'.'2Fs'.'bF91c'.'2VyX2Z1b'.'mM='),base64_decode('Y2F'.'sbF9'.'1c'.'2VyX2Z1bmM='),base64_decode(''.'Y2FsbF91c2VyX'.'2Z1bmM='),base64_decode('Y2F'.'sbF'.'91c2VyX2Z1'.'b'.'mM'.'='),base64_decode('Y2FsbF9'.'1c2Vy'.'X'.'2Z1bmM='));if(!function_exists(__NAMESPACE__.'\\___1749852643')){function ___1749852643($_1929900681){static $_1140630355= false; if($_1140630355 == false) $_1140630355=array('LQ==','bW'.'F'.'pbg'.'='.'=','bWF'.'pbg==','L'.'Q==','bWFpbg'.'='.'=','f'.'lBBUkFNX01BW'.'F9VU0'.'VSUw==','LQ==','bWF'.'pbg==',''.'flB'.'BUk'.'FNX0'.'1'.'BWF9VU0VS'.'Uw'.'==','Lg='.'=','S'.'Co'.'=','Ym'.'l'.'0cml4','T'.'ElDRU5'.'TRV9LR'.'Vk=','c'.'2hhMj'.'U'.'2','LQ==','bWFpbg==','flBB'.'UkFNX01BWF9V'.'U0'.'VSUw==','L'.'Q==','bWF'.'p'.'bg'.'==','U'.'EFS'.'QU1fT'.'UFYX1VTRVJ'.'T','VVN'.'FU'.'g==','VVNF'.'Ug='.'=',''.'VV'.'NFUg==','S'.'XNBd'.'XRo'.'b'.'3J'.'pemVk','V'.'VNFUg='.'=','SX'.'NBZG1'.'pbg='.'=','QVBQTElDQ'.'VRJT0'.'4=',''.'U'.'mVzdGFydEJ'.'1ZmZ'.'lcg==',''.'TG9jYWxS'.'ZWRpcmVj'.'dA==','L'.'2xp'.'Y2Vuc'.'2Vfc'.'m'.'VzdHJpY'.'3'.'Rpb24ucGhw',''.'LQ==','bWFp'.'bg==',''.'flBBU'.'kFNX01B'.'WF9VU0VSUw==','LQ==',''.'bWFpb'.'g==','U'.'EFSQU'.'1fT'.'UFYX1VTR'.'VJT','XEJ'.'pdHJpeFx'.'NYWluX'.'ENvb'.'mZpZ1xPcHRpb246OnNldA='.'=','bWFp'.'bg==','UEFSQU'.'1fTUFYX1VTR'.'VJT');return base64_decode($_1140630355[$_1929900681]);}};if(isset(self::$options[___1749852643(0)][___1749852643(1)]) && $moduleId === ___1749852643(2)){ if(isset(self::$options[___1749852643(3)][___1749852643(4)][___1749852643(5)])){ $_1487034327= self::$options[___1749852643(6)][___1749852643(7)][___1749852643(8)]; list($_1570249661, $_1626265283)= $GLOBALS['____1080607651'][0](___1749852643(9), $_1487034327); $_1946940399= $GLOBALS['____1080607651'][1](___1749852643(10), $_1570249661); $_239075269= ___1749852643(11).$GLOBALS['____1080607651'][2]($GLOBALS['____1080607651'][3](___1749852643(12))); $_1785109121= $GLOBALS['____1080607651'][4](___1749852643(13), $_1626265283, $_239075269, true); self::$options[___1749852643(14)][___1749852643(15)][___1749852643(16)]= $_1626265283; self::$options[___1749852643(17)][___1749852643(18)][___1749852643(19)]= $_1626265283; if($GLOBALS['____1080607651'][5]($_1785109121, $_1946940399) !==(1040/2-520)){ if(isset($GLOBALS[___1749852643(20)]) && $GLOBALS['____1080607651'][6]($GLOBALS[___1749852643(21)]) && $GLOBALS['____1080607651'][7](array($GLOBALS[___1749852643(22)], ___1749852643(23))) &&!$GLOBALS['____1080607651'][8](array($GLOBALS[___1749852643(24)], ___1749852643(25)))){ $GLOBALS['____1080607651'][9](array($GLOBALS[___1749852643(26)], ___1749852643(27))); $GLOBALS['____1080607651'][10](___1749852643(28), ___1749852643(29), true);} return;}} else{ self::$options[___1749852643(30)][___1749852643(31)][___1749852643(32)]= round(0+2.4+2.4+2.4+2.4+2.4); self::$options[___1749852643(33)][___1749852643(34)][___1749852643(35)]= round(0+12); $GLOBALS['____1080607651'][11](___1749852643(36), ___1749852643(37), ___1749852643(38), round(0+2.4+2.4+2.4+2.4+2.4)); return;}}/**/
	}

	/**
	 * Sets an option value and saves it into a DB. After saving the OnAfterSetOption event is triggered.
	 *
	 * @param string $moduleId The module ID.
	 * @param string $name The option name.
	 * @param string $value The option value.
	 * @param string $siteId The site ID, if the option depends on a site.
	 * @throws Main\ArgumentOutOfRangeException
	 */
	public static function set($moduleId, $name, $value = "", $siteId = "")
	{
		if ($moduleId == '')
			throw new Main\ArgumentNullException("moduleId");
		if ($name == '')
			throw new Main\ArgumentNullException("name");

		if ($siteId === false)
		{
			$siteId = static::getDefaultSite();
		}

		$con = Main\Application::getConnection();
		$sqlHelper = $con->getSqlHelper();

		$updateFields = [
			"VALUE" => $value,
		];

		if($siteId == "")
		{
			$insertFields = [
				"MODULE_ID" => $moduleId,
				"NAME" => $name,
				"VALUE" => $value,
			];

			$keyFields = ["MODULE_ID", "NAME"];

			$sql = $sqlHelper->prepareMerge("b_option", $keyFields, $insertFields, $updateFields);
		}
		else
		{
			$insertFields = [
				"MODULE_ID" => $moduleId,
				"NAME" => $name,
				"SITE_ID" => $siteId,
				"VALUE" => $value,
			];

			$keyFields = ["MODULE_ID", "NAME", "SITE_ID"];

			$sql = $sqlHelper->prepareMerge("b_option_site", $keyFields, $insertFields, $updateFields);
		}

		$con->queryExecute(current($sql));

		static::clearCache($moduleId);

		static::loadTriggers($moduleId);

		$event = new Main\Event(
			"main",
			"OnAfterSetOption_".$name,
			array("value" => $value)
		);
		$event->send();

		$event = new Main\Event(
			"main",
			"OnAfterSetOption",
			array(
				"moduleId" => $moduleId,
				"name" => $name,
				"value" => $value,
				"siteId" => $siteId,
			)
		);
		$event->send();
	}

	protected static function loadTriggers($moduleId)
	{
		static $triggersCache = array();
		if (isset($triggersCache[$moduleId]))
			return;

		if (preg_match("#[^a-zA-Z0-9._]#", $moduleId))
			throw new Main\ArgumentOutOfRangeException("moduleId");

		$triggersCache[$moduleId] = true;

		$path = Main\Loader::getLocal("modules/".$moduleId."/option_triggers.php");
		if ($path === false)
			return;

		include($path);
	}

	protected static function getCacheTtl()
	{
		static $cacheTtl = null;

		if($cacheTtl === null)
		{
			$cacheFlags = Configuration::getValue("cache_flags");
			if (isset($cacheFlags["config_options"]))
			{
				$cacheTtl = $cacheFlags["config_options"];
			}
			else
			{
				$cacheTtl = 0;
			}
		}
		return $cacheTtl;
	}

	/**
	 * Deletes options from a DB.
	 *
	 * @param string $moduleId The module ID.
	 * @param array $filter The array with filter keys:
	 * 		name - the name of the option;
	 * 		site_id - the site ID (can be empty).
	 * @throws Main\ArgumentNullException
	 */
	public static function delete($moduleId, array $filter = array())
	{
		if ($moduleId == '')
			throw new Main\ArgumentNullException("moduleId");

		$con = Main\Application::getConnection();
		$sqlHelper = $con->getSqlHelper();

		$deleteForSites = true;
		$sqlWhere = $sqlWhereSite = "";

		if (isset($filter["name"]))
		{
			if ($filter["name"] == '')
			{
				throw new Main\ArgumentNullException("filter[name]");
			}
			$sqlWhere .= " AND NAME = '{$sqlHelper->forSql($filter["name"])}'";
		}
		if (isset($filter["site_id"]))
		{
			if($filter["site_id"] <> "")
			{
				$sqlWhereSite = " AND SITE_ID = '{$sqlHelper->forSql($filter["site_id"], 2)}'";
			}
			else
			{
				$deleteForSites = false;
			}
		}
		if($moduleId == 'main')
		{
			$sqlWhere .= "
				AND NAME NOT LIKE '~%' 
				AND NAME NOT IN ('crc_code', 'admin_passwordh', 'server_uniq_id','PARAM_MAX_SITES', 'PARAM_MAX_USERS') 
			";
		}
		else
		{
			$sqlWhere .= " AND NAME <> '~bsm_stop_date'";
		}

		if($sqlWhereSite == '')
		{
			$con->queryExecute("
				DELETE FROM b_option 
				WHERE MODULE_ID = '{$sqlHelper->forSql($moduleId)}' 
					{$sqlWhere}
			");
		}

		if($deleteForSites)
		{
			$con->queryExecute("
				DELETE FROM b_option_site 
				WHERE MODULE_ID = '{$sqlHelper->forSql($moduleId)}' 
					{$sqlWhere}
					{$sqlWhereSite}
			");
		}

		static::clearCache($moduleId);
	}

	protected static function clearCache($moduleId)
	{
		unset(self::$options[$moduleId]);

		if (static::getCacheTtl() !== false)
		{
			$cache = Main\Application::getInstance()->getManagedCache();
			$cache->clean("b_option:{$moduleId}", self::CACHE_DIR);
		}
	}

	protected static function getDefaultSite()
	{
		static $defaultSite;

		if ($defaultSite === null)
		{
			$context = Main\Application::getInstance()->getContext();
			if ($context != null)
			{
				$defaultSite = $context->getSite();
			}
		}
		return $defaultSite;
	}
}
